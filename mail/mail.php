<!DOCTYPE html>
<html>
	<head>
        <meta name="robots" content="noindex,nofollow">
		<title>Sepals - Email</title>
		<link rel="stylesheet" type="text/css" href="../stylesheet.css"/>
	</head>
    <body>
        <div class="titlecontainer">
            <a href="../index.htm">sepals</a>
        </div>
        <div class="textcontainer">
        <?php 
            require '/home/sepalsth/php/PHPMailer.php';
            require '/home/sepalsth/php/SMTP.php';
            require '/home/sepalsth/php/Exception.php';
            use PHPMailer\PHPMailer\PHPMailer;

            $sent = FALSE;

            if ($_POST['message'] != '')
            {
                $subject = $_POST['subject'];

                $headers = "From: reply-to@sepalstheband.com\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                $message = '<html><body>';
                $message .= nl2br($_POST['message']);
                $message .= '<br><br>---------------<br><br>';
                $message .= 'If you have any questions or comments, shoot us an email at <a href="mailto:sepals.the.band@gmail.com">sepals.the.band@gmail.com</a><br>';
                $message .= '<a href="https://www.sepalstheband.com/unsubscribe.htm">Unsubscribe from these emails</a>.';
                $message .= '</body></html>';


                $file = '../mail/mailing_list.txt';                    
                $emails = file($file);

                foreach ($emails as $to)
                {
                    if (send_message($to, $subject, $message))
                        $sent = TRUE;
                }
                echo "<br>";
            }

            if ($sent == TRUE)
                echo "Successfully sent email to the mailing list.";
            else
                echo "Unable to send email to the mailing list!";

            function send_message($email, $subject, $message)
            {
                $mail = new PHPMailer();

                $mail->setFrom('reply-to@sepalstheband.com', 'Sepals');
                $mail->addAddress($email);
                $mail->addReplyTo('reply-to@sepalstheband.com', 'Sepals');

                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body = $message;
    
                if (! $mail->send())
                {
                    echo "Failed to send email to '$email'<br>";
                    return FALSE;
                }
                return TRUE;
            }            
        ?>
        </div>
	</body>
</html>
