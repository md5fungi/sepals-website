<!DOCTYPE html>
<html>
	<head>
		<title>Sepals - Subscribe</title>
		<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
	</head>
    <body>
        <div class="titlecontainer">
            <a href="index.htm">sepals</a>
        </div>

        <?php 
            require '/home/sepalsth/php/PHPMailer.php';
            require '/home/sepalsth/php/SMTP.php';
            require '/home/sepalsth/php/Exception.php';
            use PHPMailer\PHPMailer\PHPMailer;

            $email = trim($_POST['email']);
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            echo "<div class=\"textcontainer\">";
            if ($email != FALSE)
            {
                $file = './mail/mailing_list.txt';                    
                
                $exists = FALSE;
                $line = '';

                $fp = fopen($file, 'r');
                while (! feof($fp))
                {
                    $line = fgets($fp);
                    if ($line == '') break;

                    $line = trim($line);
                    if ($line == $email)
                        $exists = TRUE;

                    $list .= $line . "\n";
                }
                fclose($fp);

                if ($exists == FALSE)
                {
                    if (notify_admin_of_newuser($email))
                    {
                        $list .= $email . "\n";
                        file_put_contents($file, $list);

                        send_confirmation($email);

                        echo "You have successfully subscribed to the mailing list.<br><br>";
                        echo "A confirmation email has been sent to your email address. If the email appears in your Spam folder, please mark it as 'Not Spam' to receive future communications from us.";
                    }
                }
                else
                {
                    echo "You are already subscribed to the mailing list.";
                }
            }
            else
            {
                echo "Sorry - you have entered an invalid email address!";
            }
            echo "</div>";

            function notify_admin_of_newuser($email)
            {
                $mail = new PHPMailer();

                $mail->setFrom('reply-to@sepalstheband.com', 'Sepals Admin');
                $mail->addAddress('sepals.the.band@gmail.com');
                $mail->addReplyTo('reply-to@sepalstheband.com', 'Sepals');

                $mail->isHTML(false);
                $mail->Subject = 'sepalstheband.com - New Mailing list subscription';
                $mail->Body = "$email has subscribed to the mailing list.";
                $mail->AltBody = "$email has subscribed to the mailing list.";
    
                if (! $mail->send())
                {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                    return FALSE;
                }
                return TRUE;
            }            

            function send_confirmation($email)
            {
                $mail = new PHPMailer();

                $mail->setFrom('reply-to@sepalstheband.com', 'Sepals');
                $mail->addAddress($email);
                $mail->addReplyTo('reply-to@sepalstheband.com', 'Sepals');

                $mail->isHTML(false);
                $mail->Subject = 'sepalstheband.com - Mailing list subscription confirmation';
                $mail->Body = 'You have successfully subscribed to the mailing list. Expect communications from us soon!';
    
                if (! $mail->send())
                {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                    return FALSE;
                }
                return TRUE;
            }            
        ?>
	</body>
</html>
