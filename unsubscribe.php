<!DOCTYPE html>
<html>
	<head>
		<title>Sepals - Unsubscribe</title>
		<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
	</head>
    <body>
        <div class="titlecontainer">
            <a href="index.htm">sepals</a>
        </div>

        <?php 
            $email = trim($_POST['email']);
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            echo "<div class=\"textcontainer\">";
            if ($email != FALSE)
            {
                $file = './mail/mailing_list.txt';                    
                
                $exists = FALSE;
                $line = '';

                $fp = fopen($file, 'r');
                while (! feof($fp))
                {
                    $line = fgets($fp);
                    if ($line == '') break;

                    $line = trim($line);
                    if ($line == $email)
                        $exists = TRUE;
                    else
                        $list .= $line . "\n";
                }
                fclose($fp);

                if ($exists == FALSE)
                {
                    echo "The address '$email' was not subscribed to our mailing list.";
                }
                else
                {
                    file_put_contents($file, $list);
                    echo "You are now unsubscribed from our mailing list.";
                }
            }
            else
            {
                echo "Sorry - you have entered an invalid email address!";
            }
            echo "</div>";
        ?>
	</body>
</html>
